import { WebRequestService } from '../../service/web-request.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators,FormBuilder } from "@angular/forms";
import { products } from '../../model/product';
import { refreshPage,resetForm } from '../../util/utility';


@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  reset() {
    resetForm(this.emptyForm);
  }

  onSubmit(){
    //console.log(this.emptyForm.value);
    this.product.post('products',this.emptyForm.value).subscribe(()=>{
      console.log(this.emptyForm.value);
    })
    refreshPage();
  }

  constructor(private product:WebRequestService) {
   }

  ngOnInit(): void {
  }

  emptyForm = new FormGroup({
    name:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    type:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    description:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    keyword:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    quantity:new FormControl('',[
      Validators.required,
      Validators.pattern('[0-9 ]*')
    ]),
    price:new FormControl('',[
      Validators.required,
      Validators.pattern('[0-9 ]*')
    ]),
    weight:new FormControl('',[
      Validators.required,
      Validators.pattern('[0-9 ]*')
    ]),
  })

}