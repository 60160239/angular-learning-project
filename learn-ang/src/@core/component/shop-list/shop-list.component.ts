import { Component, OnInit } from '@angular/core';

import { products } from '../../model/product';
import { WebRequestService } from '../../service/web-request.service';
import { FormControl, FormGroup, Validators,FormBuilder } from "@angular/forms";
import { refreshPage,resetForm } from '../../util/utility';


@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.css'],
})


export class ShopListComponent implements OnInit{
  tempData: any;
  productData = products;
  reset() {
    resetForm(this.editForm);
  }
  onSubmit(){
    //console.log(this.emptyForm.value);
    this.product.post('products',this.editForm.value).subscribe(()=>{
      console.log(this.editForm.value);
    })
  }
  showID(id:any){
    console.log(`${id}`);
    this.product.getOne(`products/${id}`)
  }

  onSubmitEdit(id:number){
    console.log(this.editForm.value);
    
    this.product.patch(`products/${id}`,this.editForm.value).subscribe(()=>{
      console.log(this.editForm.value);
    })
    refreshPage();
  }

  showEditForm(id:number,name:string,type:string,description:string,keyword:Array<string>,quantity:number,price:number,weight:number){
    this.editForm.setValue({
      id: id, 
      name: name,
      type: type,
      description: description,
      keyword: keyword,
      quantity: quantity,
      price: price,
      weight: weight
    });
  }

  deleteProduct(id:number){
    //console.log(`Delete product with ID:${id}`);
    this.product.delete(`products/${id}`).subscribe((deletedData)=>{
      console.log(deletedData);
      
    });
    this.product.deleteTest()
    refreshPage();
  }

  constructor(private product:WebRequestService){
  }

  ngOnInit(): void {
    this.product.get('products').subscribe((productDataIn)=>{

      this.tempData = productDataIn;
      this.productData = this.tempData;
      this.productData.sort((a,b)=> a._id-b._id); //sort by manually created ID
    });
  }
  editForm = new FormGroup({
    id:new FormControl(),
    name:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    type:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    description:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    keyword:new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(30),
    ]),
    quantity:new FormControl('',[
      Validators.required,
      Validators.pattern('[0-9 ]*')
    ]),
    price:new FormControl('',[
      Validators.required,
      Validators.pattern('[0-9 ]*')
    ]),
    weight:new FormControl('',[
      Validators.required,
      Validators.pattern('[0-9 ]*')
    ]),
  })
  
}
