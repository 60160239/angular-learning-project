import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopListComponent } from '../shop-list/shop-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/product', pathMatch: 'full'},
  { path: 'product', component: ShopListComponent},
  { path: 'product/:productId', component: ShopListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
