import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class WebRequestService {

  readonly ROOT_URL;

  constructor(private http: HttpClient) {
    this.ROOT_URL = 'http://localhost:3000';
   }

  get(uri: string){
    console.log(`get from ${this.ROOT_URL}/${uri}`)
     return this.http.get(`${this.ROOT_URL}/${uri}`);
  }

  getOne(uri: string){
    console.log(`get one from ${this.ROOT_URL}/${uri}`)
    return this.http.get(`${this.ROOT_URL}/${uri}`);
  }

  post(uri: string, payload: Object){
    console.log(`post to ${this.ROOT_URL}/${uri}`)
    return this.http.post(`${this.ROOT_URL}/${uri}`,payload);
  }

  patch(uri: string, payload: Object){
    console.log(`patch to ${this.ROOT_URL}/${uri}`)
    return this.http.patch(`${this.ROOT_URL}/${uri}`,payload);
  }

  delete(uri: string){
    console.log(`delete in ${this.ROOT_URL}/${uri}`)
    return this.http.delete(`${this.ROOT_URL}/${uri}`);
  }

  deleteTest(){
    console.log(`delete in ${this.ROOT_URL}`)
    return this.http.delete(`${this.ROOT_URL}`);
  }
  
}
