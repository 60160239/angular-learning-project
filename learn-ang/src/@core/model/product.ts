export interface Product {
  name: string,
  type: string,
  description: string,
  keyword: Array<any>,
  quantity: number,
  price: number,
  weight: number,
}

export const products = [
  {
    _id: 1,
    //_id : new ObjectId('asdasdasd123'),
    id: 1,
    name: 'Phone XL',
    type: 'Phone',
    keyword: ['Phone'],
    quantity: 100,
    weight: 5,
    price: 799.66,
    description: 'A large phone with one of the best screens'
  },
  {
    //_id : ObjectID('aqweqwe12345'),
    _id: 4,
    name: 'Porkchopchop',
    type: 'Food',
    description: 'Porkchop that got chop',
    keyword: ['Pork','Food','Can be expired'],
    quantity: 50,
    price: 100,
    weight: 1.5,
  }
];
