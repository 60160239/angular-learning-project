import { FormGroup } from '@angular/forms';
export function refreshPage(){
  window.location.reload();
}

export function resetForm(formGroup:FormGroup){
  formGroup.reset();
}