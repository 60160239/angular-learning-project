const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true,
    trim: true
  }
},{
  collection: 'product'
})

const product = mongoose.model('product',productSchema);

module.exports = { product }