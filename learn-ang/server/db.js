const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/product-crud', { useNewUrlParser: true}).then(() =>{
  console.log('Database connected');
}).catch((e)=>{
  console.log('Database not connected');
  console.log(e);
});

module.exports = {
  mongoose
}