const mongoose = require('mongoose');

const addSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  type:{
    type: String,
    required: true,
    trim: true
  },
  description: String,
  keyword: [String],
  quantity: {
    type: Number,
    required: true,
    trim: true
  },
  price: {
    type: Number,
    required: true,
    trim: true
  },
  weight: {
    type: Number,
    required: true,
    trim: true
  }
},{
  collection: 'product'
})

const addProduct = mongoose.model('addProduct',addSchema);

module.exports = { addProduct }